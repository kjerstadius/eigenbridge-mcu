#define AMP_STDBY_PORT  PORTB
#define AMP_STDBY_PIN   0
#define SMPS_STDBY_PORT PORTB
#define SMPS_STDBY_PIN  1
#define BUTTON_PORT     PORTA
#define BUTTON_PIN      4
#define LED_PORT        PORTA
#define LED_PIN         5
#define TRIGGER_PORT    PORTA
#define TRIGGERA_PIN    2
#define TRIGGERB_PIN    3

// Slight trick to force expansion of macro arguments
// See https://gcc.gnu.org/onlinedocs/cpp/Argument-Prescan.html
#define _pin_bm(pin_nr) PIN##pin_nr##_bm
#define pin_bm(pin_nr)  _pin_bm(pin_nr)

#define _PIN_CTRL(pin_nr) PIN##pin_nr##CTRL
#define PIN_CTRL(pin_nr)  _PIN_CTRL(pin_nr)
