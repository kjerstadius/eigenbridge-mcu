#include "clock.h"
#include <avr/io.h>


void init_clock(void)
{
    CCP               = CCP_IOREG_gc;
    CLKCTRL.MCLKCTRLB = CLKCTRL_PDIV_8X_gc;
}
