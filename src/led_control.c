#include "clock.h"
#include "pins.h"

#include <avr/io.h>


#define F_LED_PWM             1000
#define LED_DUTY_CYCLE_BRIGHT 0.25f
#define LED_DUTY_CYCLE_DIM    0.005f
#define TCD_CLK_PRESCALE      4
#define F_TCD_CLK             F_CPU / TCD_CLK_PRESCALE
#define CMPB_CNT_END          F_TCD_CLK / F_LED_PWM
#define INPUT_BLANK_PRESCALE  8
#define INPUT_BLANK_MS        4


static void set_led_duty_cycle(float duty_cycle, uint8_t sync)
{
    if (sync) {
        uint8_t ready = 0;
        while (ready == 0) {
            ready = TCD0.STATUS & TCD_CMDRDY_bm;
        }

        TCD0.CMPBSET = (uint16_t)((1.0f - duty_cycle) * (float)CMPB_CNT_END);
        TCD0.CTRLE |= TCD_SYNCEOC_bm;
    }
    else {
        TCD0.CMPBSET = (uint16_t)((1.0f - duty_cycle) * (float)CMPB_CNT_END);
    }
}


void set_led_bright(void)
{
    set_led_duty_cycle(LED_DUTY_CYCLE_BRIGHT, 1);
}


void set_led_dim(void)
{
    set_led_duty_cycle(LED_DUTY_CYCLE_DIM, 1);
}


void init_led_pwm(void)
{
    LED_PORT.DIRSET = pin_bm(LED_PIN);

    // Static register configuration
    // See page 201 for classification
    TCD0.CTRLA     = TCD_CLKSEL_SYSCLK_gc | TCD_CNTPRES_DIV4_gc;
    TCD0.CTRLB     = TCD_WGMODE_ONERAMP_gc;
    CCP            = CCP_IOREG_gc;
    TCD0.FAULTCTRL = TCD_CMPBEN_bm;

    uint8_t input_blank_cycles =
        INPUT_BLANK_MS * (F_TCD_CLK / INPUT_BLANK_PRESCALE) / 1000;

    // Double buffered register configuration
    TCD0.CMPACLR = 0;
    TCD0.CMPASET = 0;
    TCD0.CMPBCLR = CMPB_CNT_END;
    TCD0.CMPBSET = CMPB_CNT_END;
    TCD0.DLYCTRL = TCD_DLYPRESC_DIV8_gc | TCD_DLYSEL_INBLANK_gc;
    TCD0.DLYVAL  = input_blank_cycles;

    uint8_t ready = 0;
    while (ready == 0) {
        ready = TCD0.STATUS & TCD_ENRDY_bm;
    }

    TCD0.CTRLA = TCD_ENABLE_bm;
}
