#include "clock.h"
#include "led_control.h"
#include "pins.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>


#define AMP_STATE_OFF 0
#define AMP_STATE_ON  1
#define AMP_PSU_DELAY 2000


static volatile uint8_t amp_state;
static volatile uint8_t toggle_state;


void init_interrupts(void)
{
    BUTTON_PORT.DIRCLR  = pin_bm(BUTTON_PIN);
    TRIGGER_PORT.DIRCLR = pin_bm(TRIGGERA_PIN);

    BUTTON_PORT.PIN_CTRL(BUTTON_PIN) =
        PORT_ISC2_bm | PORT_ISC0_bm | PORT_PULLUPEN_bm;
    TRIGGER_PORT.PIN_CTRL(TRIGGERA_PIN) =
        PORT_INVEN_bm | PORT_ISC0_bm | PORT_PULLUPEN_bm;

    sei();
}


ISR(PORTA_PORT_vect)
{
    uint8_t intflags = PORTA.INTFLAGS;
    // Clear interrupt bits
    PORTA.INTFLAGS = intflags;

    uint8_t int_from_button  = (intflags & pin_bm(BUTTON_PIN)) >> BUTTON_PIN;
    uint8_t int_from_trigger =
        (intflags & pin_bm(TRIGGERA_PIN)) >> TRIGGERA_PIN;
    uint8_t trigger_state =
        (TRIGGER_PORT.IN & pin_bm(TRIGGERA_PIN)) >> TRIGGERA_PIN;

    toggle_state =
        (int_from_trigger & (trigger_state ^ amp_state)) | int_from_button;
}


void init_pins(void)
{
    // Output pins
    AMP_STDBY_PORT.DIRSET  = pin_bm(AMP_STDBY_PIN);
    SMPS_STDBY_PORT.DIRSET = pin_bm(SMPS_STDBY_PIN);
}


void amp_off(void)
{
    set_led_dim();
    AMP_STDBY_PORT.OUTSET = pin_bm(AMP_STDBY_PIN);
    _delay_ms(AMP_PSU_DELAY);
    SMPS_STDBY_PORT.OUTSET = pin_bm(SMPS_STDBY_PIN);
    amp_state              = AMP_STATE_OFF;
}


void amp_on(void)
{
    set_led_bright();
    SMPS_STDBY_PORT.OUTCLR = pin_bm(SMPS_STDBY_PIN);
    _delay_ms(AMP_PSU_DELAY);
    AMP_STDBY_PORT.OUTCLR = pin_bm(AMP_STDBY_PIN);
    amp_state             = AMP_STATE_ON;
}


int main(void)
{
    init_clock();
    init_interrupts();
    init_led_pwm();
    init_pins();

    amp_off();
    toggle_state = 0;

    while (1) {
        if (toggle_state) {
            if (amp_state == AMP_STATE_ON)
                amp_off();
            else
                amp_on();
            toggle_state = 0;
        }
        _delay_ms(25);
    }

    return 1;
}
