Eigenbridge
===========
The Eigenbridge is a simple MCU board for connecting and controlling a [Purifi 1ET400A Eigentakt
EVAL1](https://purifi-audio.com/eigentakt/eval1) kit to a Hypex SMPS, such as the [SMPS
1200A400](https://www.hypex.nl/products/smps-family/smps1200a400). It has the following main features:

* Control SMPS and amplifier on/off signals based on trigger input and/or front panel button
* Simplify integration of Purifi Eigentakt eval kit with Hypex SMPS by acting as an interposer
* LED PWM dimming for front panel indicator (for example)
* Shut down SMPS in case of amp module error

This repo contains the code for the MCU of the Eigenbridge board, which uses an
[ATtiny414](https://www.microchip.com/en-us/product/ATtiny414).

Build
=====
```
meson build -Dbuildtype=minsize --cross-file cross-avr-attiny414.txt
ninja -C build
```

Flash
=====
```
pyupdi -d attiny414 -c /dev/ttyUSB0 -f build/src/eigenbridge.hex
```



